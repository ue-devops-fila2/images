# Hébergement des images de base

## Liste des images disponibles
- hello-world
- whoami
- nginx
- python
- golang
- dotnet-sdk
- dotnet-runtime
- dotnet-runtime-deps

## Utilisation des images

Gitlab héberge ces images sur son registre d'images.

L'URL de base est : `gitlab-registry.imt-atlantique.fr/ue-devops-fila2/images/`

A cet URL il faudra ajouter le nom de l'image à utiliser, par exemple :

```bash
docker pull gitlab-registry.imt-atlantique.fr/ue-devops-fila2/images/whoami
docker run gitlab-registry.imt-atlantique.fr/ue-devops-fila2/images/whoami
```


Ou dans un fichier docker compose :

```yaml
services:
    whoami:
        image: gitlab-registry.imt-atlantique.fr/ue-devops-fila2/images/whoami
        ports:
            - 80:80

```